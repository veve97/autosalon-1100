public class Vozidlo {
    private String barva;
    private String SPZ;

    public Vozidlo(String barva, String SPZ) {
        this.barva = barva;
        this.SPZ = SPZ;
    }

    public String getBarva() {
        return barva;
    }

    public void setBarva(String barva) {
        this.barva = barva;
    }

    public String getSPZ() {
        return SPZ;
    }

    public void setSPZ(String SPZ) {
        this.SPZ = SPZ;
    }

    @Override
    public String toString() {
        return
                "barva='" + barva + '\'' +
                ", SPZ='" + SPZ + '\''
                ;
    }
}
