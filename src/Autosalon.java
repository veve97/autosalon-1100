import java.util.ArrayList;

public class Autosalon {
    private String nazev;
    private ArrayList<Vozidlo> vozidla = new ArrayList<>();

    public Autosalon(String nazev) {
        this.nazev = nazev;
    }

    public String pridatVozidlo(Vozidlo vozidlo){
        if (vyhledejVozidlo(vozidlo.getSPZ()) == null){
            vozidla.add(vozidlo);
            return "pridano vozidlo "+ vozidlo;
        }
        return "Vozidlo s SPZ:"+vozidlo.getSPZ()+" již je v autosalonu.";
    }
    public Vozidlo vyhledejVozidlo(String SPZ){
        for (Vozidlo vozidlo: vozidla
             ) {
            if (SPZ.equals(vozidlo.getSPZ())){
                return vozidlo;
            }

        }
        return null;
    }

    public String odeberVozidlo(String SPZ){
        Vozidlo nalezenoVozidlo = vyhledejVozidlo(SPZ);
        if (nalezenoVozidlo != null){
            vozidla.remove(nalezenoVozidlo);
            return "Odebrano vozidlo s SPZ "+ SPZ;
        }
        return "Vozidlo s SPZ "+ SPZ+"se v autosalonu nenachazi.";
    }

    @Override
    public String toString() {
        return "Autosalon{" +
                "nazev='" + nazev + '\'' +
                ", vozidla=" + vozidla +
                '}';
    }
}
