public class Main {
    public static void main(String[] args) {
        Automobil auto1 = new Automobil("cerna","AAA",'B');
        Motocykl moto1 = new Motocykl("bila","BBB",3);
        Autosalon salon1 = new Autosalon("Autosalon");
        System.out.println(auto1.getBarva());
        System.out.println(auto1);

        System.out.println(salon1.pridatVozidlo(auto1));
        System.out.println(salon1.pridatVozidlo(moto1));
        System.out.println(salon1.vyhledejVozidlo("AAA"));
        System.out.println(salon1.vyhledejVozidlo("CCC"));
        System.out.println(salon1);
        System.out.println(salon1.odeberVozidlo("AAA"));
        System.out.println(salon1);
        System.out.println(salon1.vyhledejVozidlo("AAA"));
    }
}