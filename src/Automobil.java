public class Automobil extends Vozidlo{
    private char ridicak;

    public Automobil(String barva, String SPZ, char ridicak) {
        super(barva, SPZ);
        this.ridicak = ridicak;
    }

    public char getRidicak() {
        return ridicak;
    }

    public void setRidicak(char ridicak) {
        this.ridicak = ridicak;
    }

    @Override
    public String toString() {
        return "Automobil{" + super.toString() +
                ", ridicak=" + ridicak +
                '}';
    }
}
