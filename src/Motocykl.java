public class Motocykl extends Vozidlo{
    private int pocetKol;

    public Motocykl(String barva, String SPZ, int pocetKol) {
        super(barva, SPZ);
        this.pocetKol = pocetKol;
    }

    public int getPocetKol() {
        return pocetKol;
    }

    public void setPocetKol(int pocetKol) {
        this.pocetKol = pocetKol;
    }

    @Override
    public String toString() {
        return "Motocykl{" + super.toString()+
                ", pocetKol=" + pocetKol +
                '}';
    }
}
